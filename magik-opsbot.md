# Magik Opsbot

Hướng dẫn sử dụng Magik Opsbot để triển khai với các lợi ích
- Tự động deploy từ git chỉ với 1 dòng lệnh.
- Tự động backup lên git.
- Tự động rotate log access và log error để tránh đầy server.

#### (1) Chuẩn bị máy chủ cloud
Máy chủ cấu hình nào cũng ok.

#### (2) Bật chiêu cuối
```sh
sudo -s
```

#### (3) Tạo một ssh key để chứng thực tài khoản gitlab.

```
echo "Gen a ssh key"
mkdir ~/.ssh
yes n | ssh-keygen -t rsa -b 4096 -C "" -N "" -f ~/.ssh/id_rsa

echo "Run ssh-agent"
eval "$(ssh-agent -s)"

echo "Add ssh key"
ssh-add ~/.ssh/id_rsa
```

Copy nội dung id_rsa.pub và thêm public key vào gitlab. Cấu hình tại link https://gitlab.com/profile/keys

```sh
cat ~/.ssh/id_rsa.pub
```

#### (4) Cài đặt Opsbot
```sh
apt install -y python-pip
pip install setuptools wheel opsbot
```

#### (5) Chuẩn bị file config.opsbot

Nếu  có sẵn rồi thì clone về
```sh
git clone git_repo_chua_config_opsbot
```

Nếu chưa có thì tạo mới bằng lệnh
```
git init
```
Sau đó edit lại cho phù hợp
```
nano config.opbot
```

#### (6) Build opsbot
```sh
opsbot build
```

#### (7) Chạy script cài đặt VM
```sh
 ./opsbotgenerated.sh
```


#### (8) Triển khai tự động các website

Tới bước này sẽ có 2 lựa chọn. 
- Một là logout khỏi root, và login bằng các tài khoản appuser để deploy từng website
- Cách thứ hai là root tự động hóa triển khai. mỗi website chỉ cần 1 lệnh. như bên dưới.

```sh
/var/www/{ten_website}/tool/deploysite.sh
```

deploysite.sh sẽ tự động hóa việc cài đặt bao gồm:
- pull source db vào /db
- pull source code vào /html
- import db (cấu hình trong db-git/dblist.opsbot)
- chạy lệnh cấu hình source code (theo cấu hình trong git-source-code/autodeploy.opsbot)

#### (9) Testing

#### (10) Sau khi hoàn tất
1. Lưu lại password
>Password trong file ./configgenerated

2. Triển khai các dự án

Nếu không chọn tự động hóa ở bước (8), thì ở bước này bạn đăng nhập lần lượt vào các tài khoản đã tạo để triển khai từng dự án.

3. Chạy Certbot
