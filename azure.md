# Azure tinh túy


----

## 3 Gói Subscription kèm khuyến mãi của Azure 

### BizSpark : Gói hỗ trợ doanh nghiệp startup.

3 năm. Được cấp 5 tài khoản. Mỗi tài khoản được budget 150$/tháng

### DreamSpark : Gói hỗ trợ Sinh viên.

- Budget 100$ không giới hạn thời gian sử dụng.
- 12 tháng xài free các dịch vụ cơ bản (Máy chủ B1s, 64Gb SSD , 1 IP, 15Gb Bandwidth)
- Không yêu cầu thẻ tín dụng.

### Free Trial : Gói hỗ trợ người dùng mới.

- Budget 200$ xài trong 1 tháng.
- 12 tháng xài free các dịch vụ cơ bản (Máy chủ B1s, 64Gb SSD , 1 IP, 15Gb Bandwidth)
- Yêu cầu thẻ tín dụng để kích hoạt.


----

## Kinh nghiệm đăng kí gói Free Trial.

1. Dùng tài khoản Microsoft Live ID đã dùng trước đó.
2. Tên tài khoản trùng với Tên trên VISA.
3. Số điện thoại không trùng.

----

## Một vài lưu ý đăng kí DreamSpark

1. Sinh viên UIT từ năm thứ 5 có thể không còn đăng kí được.
2. 

----

