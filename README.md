# Magik Ops Standard

> Tôi có một giấc mơ! Đó là trong cuộc đời dài dằng dặc này, tất cả những gì tôi cần nhớ về Dev Ops, chỉ là đường link website này. OPS.magik.vn <br/>


Đây là một trang tổng hợp các tiêu chuẩn và hướng dẫn về triển khai và bảo mật các sản phẩm công nghệ từ USM - [The United Startups of Magik](http://magik.vn)

Trong nhiều năm qua, Những sự cố mà Magik thường gặp là:
1. Server down không rõ nguyên nhân. Dịch vụ gián đoạn.
2. Bị Hack / DDOS / Cài backdoor / Botnet / Dính virus đòi tiền chuộc.
3. Mất quyền truy cập máy chủ. Mất dữ liệu/database người dùng.
4. Lỡ tay xóa mất dữ liệu/database người dùng.
5. Bị leak mã nguồn ra bên ngoài, kẻ xấu build thành ứng dụng khác up lên store.

Những sự cố tiềm ẩn:
1. Bị hack tài khoản Azure, Kẻ xấu tạo máy ảo cày tiền ảo, gửi thư rác.

Do đó, Chúng tôi tổng hợp những tiêu chuẩn dưới đây, nhằm loại bỏ hầu hết các rủi ro tiềm ẩn, giúp cho USM có thể vận hành các sản phẩm công nghệ với chất lượng dịch vụ tốt nhất.

----

## Tiêu chuẩn triển khai hệ thống

Mười nguyên tắc về triển khai hệ thống để đảm bảo bảo mật

**Nguyên tắc 1: Chọn OS phiên bản Long Time Support mới nhất**

Các gói Long Time Support, nhà cung cấp sẽ cung cấp các bản vá bảo mật thường xuyên

**Nguyên tắc 2: Cài ít gói nhất có thể**

Cứ mỗi một software cài đặt lên hệ thống, Server mình lại gánh thêm rủi ro bảo mật đến từ những lỗ hổng tiềm ẩn của phần mềm đó.

**Nguyên tắc 3: SSH với user có quyền thấp nhất có thể**

User có quyền càng thấp, thì khi bị lộ, khả năng phá hoại càng nhỏ
 
> Root (superadmin) chỉ được dùng trong giai đoạn setup ban đầu. Hạn chế tối đa dùng root để deploy.

**Nguyên tắc 4: Mỗi product một "ổ khóa" riêng**

Khi 1 dịch vụ / sản phẩm bị xâm nhập, Các product khác vẫn được an toàn.

>Tạo ra các tài khoản có quyền hạn chế cho mỗi dịch vụ, hoặc mỗi đối tượng người dùng.

**Nguyên tắc 5: Password tạo ngẫu nhiên, chỉ dùng 1 lần**

- Password chỉ cần có quy tắc tạo ra thì đã có thể bruteforce.
- Password xài lại thì lộ chỗ này, chỗ khác sẽ lâm nguy.

> Dùng các trang web randompassword. Sau đó tự tay thay đổi 1 chút.

**Nguyên tắc 6: Gõ lệnh ít thì sai sót ít. Gõ lệnh nhiều thì sai sót nhiều. Không gõ thì không bao giờ sai**

Tự động hóa quy trình bằng Bash Script. Đừng cố nhớ các bước devops trong đầu. thay vào đó hãy tài liệu hóa nó. tự động hóa bằng bash script.

**Nguyên tắc 7: Phải ghi log hệ thống**

AccessLog / ErrorLog là 2 loại log kinh điển nhất, và là đủ để điều tra các sự cố diễn ra trên server.
Xóa AccessLog định kì mỗi khi kích thước > 500 Mb

**Nguyên tắc 8: Phải có backup**
- Database và các hình ảnh upload mới của người dùng cần được sao lưu phòng trường hợp Bị mất quyền truy cập vào server.
- Với những dữ liệu quan trọng, có thể backup hằng ngày/ còn thì có thể hàng tuần.
- Công việc backup có thể tự động hóa bằng cronjob / git.

----

## Tiêu chuẩn quản lý mã nguồn

**Nguyên tắc 1 : Gitlab là dịch vụ quản lý source code cuối cùng của Magik**
- GitLab sẽ như Facebook. Miễn phí vĩnh viễn.
- GitHub sử dụng để chia sẻ các thư viện mã nguồn mở

**Nguyên tắc 2 : Chia Git Project càng nhỏ cái tốt**

- The GOOD: API 1 git riêng. Android 1 Git riêng. iOS 1 Git Riêng. Devops một git riêng.
- The BAD: 1 Git chung chứa cả ANDROID / IOS / Database

**Nguyên tắc 3 : Mã nguồn sản phẩm, phân quyền cho càng ít người truy cập càng khó bị lộ**

- Thực tập của team chỉ được phân quyền Guest
- Thành viên của team làm dự án nào thì Role Developer dự án đó.

**Nguyên tắc 4 : Mã nguồn thư viện, Open source cho cộng đồng càng nhiều thì càng tạo ra giá trị**

- Phần code nào sử dụng nhiều thì nên tách thành library.
- Library nếu không phải là công nghệ độc quyền thì nên chia sẻ cho cộng đồng.

> Vì mình đã xài rất nhiều free library từ cộng đồng


**Nguyên tắc 5 : Không lưu trữ username/password vào trong git**

> Password Git rất dễ bị lộ.


