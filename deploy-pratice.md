# Devops thực hành

----

## 12 bước triển khai kinh điển.
1. Tạo một máy chủ ảo trên Azure
2. Cài đặt các gói platform lập trình cần thiết
    * Linux - Apache - MySQL - PHP
    * Node - NPM - Express - Mongo
    * Composer - Codeigniter
3. Cài đặt Certbot (để hỗ trợ HTTPS)
4. Tạo và phân quyền người dùng Unix
5. Tạo và phân quyền người dùng các dịch vụ : Mysql / MongoDB
6. Tạo các sites (virtual host) và phân quyền.
7. Lưu trữ thông tin truy cập dịch vụ vào một nơi an toàn.
8. Triển khai các dịch vụ dựa trên các git devops. (Bằng tài khoản hạn chế quyền truy cập)
9. Test thử bằng Fake DNS 
10. **Chỉnh domain DNS trỏ về Server mới**, Lúc này server mói chính thức trở thành server live.
11. Chạy cerbot để xin cấp phát SSL Certifice trực tiếp.


Và thế là xong.

----

## Snippet dùng trong quá trình triển khai

### Cài đặt Apache / PHP / Mysql
```sh
apt install lamp_server^
```

### Cài đặt NodeJS
```sh
apt update
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
apt install -y nodejs
```

### Cài đặt Certbot

```sh
apt install --assume-yes software-properties-common
add-apt-repository ppa:certbot/certbot
apt update
apt install --assume-yes python-certbot-apache 
```

### Cài đặt composer
```sh
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
```

### Tạo 1 người dùng linux với username là diepnh
```sh
adduser diepnh
```

### Tạo 1 người dùng mysql
```sh
username=diepnh
password=khongcopass
sql1="CREATE USER '$username'@'%' IDENTIFIED WITH mysql_native_password BY  '$password';"
mysql -uroot -p"$rootpassword" --execute="$sql1"
```

### Phân quyền cho 1 người dùng full quyền trên các database có 1 prefix xác định

Tương tự cái trên, tuy nhiên, thay sql1 bằng:
```sh
dbprefix=mydb_
sql1="GRANT ALL PRIVILEGES ON \`${dbprefix}%\`.* TO '$username'@'%' WITH GRANT OPTION;flush privileges;"
```

###  Chạy certbot để xin cấp SSL Certificate

```sh
certbot --apache
```

### Fake DNS
Trên windows, edit file 
> C:\Windows\System32\drivers\etc\hosts

### Tạo 1 virtual host

File mẫu:

```
<VirtualHost *:80>
    ServerName english.magik.vn

    ServerAdmin diepnh@magik.vn

    DocumentRoot /var/www/html/english.magik.vn/public_html
    <Directory /var/www/html/english.magik.vn>
        AllowOverride All
    </Directory>

    ErrorLog  /var/www/html/english.magik.vn/error.log
    CustomLog /var/www/html/english.magik.vn/access.log combined

</VirtualHost>
```


