# 
1. Tấn công xuyên domain.
User khác có thể thông qua quyền từ group www-data để xem nội dung các website khác trên hệ thống.

2. Reset owner thủ công.
Những file trong thư mục /var/www/website.com/html có owner là user1 thì khi các user khác tạo file trong thư mục này, user1 sẽ ko có quyền xóa.