# Linux tinh túy

Mục lục
- [10 lệnh kinh điển nhất của Linux](#10-lệnh-kinh-điển-nhất-của-linux)
- [4 thư mục quan trọng nhất trên Linux Ubuntu](#4-thư-mục-quan-trọng-nhất-trên-linux-ubuntu)
- [10 lệnh căn bản của Devops](#10-lệnh-căn-bản-của-devops)
- [Hiểu về CHMOD ](#hiểu-về-chmod)

----
## 10 lệnh kinh điển nhất của Linux

### man
Xem hướng dẫn sử dụng của lệnh.
>man ls để xem cách sử dụng lệnh ls.

>ls --help cũng có tác dụng tương tự.


### ls
Xem danh sách các các file, thư mục con trong một thư mục.
> ls -l : sẽ xem chi tiết hơn. Ví dụ owner, mode của file.

### cd
chuyển sang thư mục khác.

> cd .. : chuyển lên thư mục cha.

### mv
Di chuyển một file/ thư mục sang vị trí khác.

### cp
Sao chép nội dung 1 file sang 1 file khác.

### mkdir
Tạo một thư mục.

### rmdir -rf
Xóa một thư mục , -rf tức là xoá không cần hỏi.

### touch
Tạo một empty file.

### clear
xóa màn hình bash shell.

### Hậu tố : > {OUTPUT_FILE}
Dấu  > {OUTPUT_FILE} đặt ở cuối lệnh để nội dung của lệnh sẽ được ghi đè vào OUTPUT_FILE

### Hậu tố : >> {OUTPUT_FILE}
Dấu  >> {OUTPUT_FILE} đặt ở cuối lệnh để nội dung của lệnh sẽ được ghi thêm vào OUTPUT_FILE

----
## 4 thư mục quan trọng nhất trên Linux Ubuntu.

### /home/{USER_NAME}
Thư mục mặc định của một Unix User.
User có toàn quyền đọc/ghi/excute mọi file trong thư mục này.

### /var/www/html
Thư mục Root Directory của Apache.
Nơi mặc định chưa mã nguồn các website để truy cập từ dịch vụ HTTP.

### /etc/apache2/sites-enabled

Thư mục cấu hình các website phục vụ bởi server này. Cấu hình thư mục root của từng website. và nơi ghi log.

### /etc/apache2/sites-available

Để enable một site, đầu tiền phải tạo 1 file conf virtual host trong thư mục này, đặt tên là site_name.conf sau đó gọi lệnh

```sh
a2ensite site_name
```

Thì site_name.conf sẽ được copy shortcut qua thư mục sites-enabled, và sau đó thì virtual host website sẽ được enable.

----

## 10 lệnh căn bản của Devops

### apt
Cài đặt các gói.

> Ví dụ 2 lệnh ngắn gọn dưới đây sẽ cài đặt một môi trường đầy đủ để chạy php web.
```sh 
apt install lamp_server^
apt install phpmyadmin
```

### adduser
Tạo một tài khoản user thường trong hệ thống.

### chown
Thiết lập owner / group của một file hoặc thư mục


### chmod 
Phân quyền đọc / ghi / thực thi cho một file / thư mục.

### nano
Edit một text file

### git
Dùng để pull source code về khi triển khai.

### wget
Download 1 file

### curl
Mở một website.

### contab -e
Thiết lập hẹn giờ thực hiện tác vụ.

### free :
Xem máy chủ còn bao nhiêu RAM

----

## Hiểu về CHMOD


